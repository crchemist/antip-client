#-------------------------------------------------
#
# Project created by QtCreator 2012-01-09T19:37:15
#
#-------------------------------------------------

QT       += network

TARGET = antip-client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
